const describe = require('mocha').describe;
const expect = require('must');
const it = require('mocha').it;
const request = require('supertest');
const server = require('../../../app');

describe('controllers', () => {
  describe('comments', () => {
    describe('GET /comments', () => {
      it('fails without parameters', (done) => {
        request(server)
          .get('/comments')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(400)
          .end((err, res) => {
            expect(err).to.be.null();
            expect(res.body.message).to.equal('Request validation failed: Parameter (key) is required');

            done();
          });
      });

      it('returns comments for given key', (done) => {
        const beforeRequest = new Date();
        request(server)
          .get('/comments')
          .query({ key: 'some key' })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end((err, res) => {
            expect(err).to.be.null();
            expect(res.body).to.be.an.array();
            expect(res.body).to.have.length(1);

            const firstComment = res.body[0];
            expect(firstComment.author).to.be.an.object();
            expect(firstComment.author.name).to.equal('Winnie');
            expect(firstComment.dateTime).to.be.a.string();
            expect(Date.parse(firstComment.dateTime)).to.be.between(beforeRequest, new Date());
            expect(firstComment.text).to.be.a.string();
            expect(firstComment.text).to.equal('Comment for some key');

            done();
          });
      });
    });
  });
});
