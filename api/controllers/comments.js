

function getComments(req, res) {
  const key = req.swagger.params.key.value;
  const comments = [
    {
      author: {
        name: 'Winnie',
      },
      dateTime: new Date().toISOString(),
      text: `Comment for ${key}`,
    },
  ];

  res.json(comments);
}

module.exports = {
  getComments,
};

