# Gimment

[![build status](https://gitlab.com/gimment/gimment/badges/master/build.svg)](https://gitlab.com/gimment/gimment/commits/master)

Gimment is

- the ribbit of a frog which bit off more than it can chew
- a markup agnostic comment system based on Git
- a [famous song]: <pre>Gimment! Gimment! Gimment! Comment after midnight.</pre>

[famous song]: https://www.youtube.com/watch?v=XEjLoHdbVeE