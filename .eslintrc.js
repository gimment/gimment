const config = {
    "extends": "airbnb-base"
};

if (process.env.CI && process.env.NYC_CONFIG) {
  config.excludedFiles = '*';
  console.error('Skipping linting for coverage analysis on CI');
}

module.exports = config;
